//#import "CCUIContentModuleContainerViewController.h"

@protocol CCUIContentModuleContentViewController <NSObject>
@property (nonatomic,readonly) CGFloat preferredExpandedContentHeight; 
@property (nonatomic,readonly) CGFloat preferredExpandedContentWidth; 
//@property (nonatomic,readonly) BOOL providesOwnPlatter; 
//@property (nonatomic,readonly) double preferredExpandedContinuousCornerRadius; // 13,14
//@property (nonatomic,readonly) UIViewPropertyAnimator * customAnimator; // 13,14
//@property (nonatomic,readonly) BOOL shouldPerformHoverInteraction; // 14
//@property (nonatomic,readonly) BOOL shouldPerformClickInteraction; // 14
@optional
- (BOOL)shouldBeginTransitionToExpandedContentModule;
- (void)willResignActive;
- (void)willBecomeActive;
- (void)willTransitionToExpandedContentMode:(BOOL)willTransition;
- (BOOL)shouldFinishTransitionToExpandedContentModule;
- (void)didTransitionToExpandedContentMode:(BOOL)didTransition;
- (BOOL)canDismissPresentedContent;
- (void)dismissPresentedContentAnimated:(BOOL)animated completion:(id)completion;
- (CGFloat)preferredExpandedContentWidth;
- (BOOL)providesOwnPlatter;
- (void)controlCenterWillPresent;
- (void)controlCenterDidDismiss;

-(void)displayWillTurnOff; // 13,14
-(BOOL)shouldExpandModuleOnTouch:(id)arg1; // 14
//-(UIViewPropertyAnimator *)customAnimator; // 13,14
//-(BOOL)shouldPerformHoverInteraction; // 14
//-(double)preferredExpandedContinuousCornerRadius; // 13,14
//-(CCUIModuleLayoutSize*)moduleLayoutSizeForOrientation:(long long)arg1; // 14
//-(BOOL)shouldPerformClickInteraction; // 14

@required
//- (CGFloat)preferredExpandedContentHeight;
-(double)preferredExpandedContentHeight; // 11-14

@end
