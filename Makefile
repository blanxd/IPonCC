export ARCHS = arm64 arm64e
export TARGET = iphone:11.0

export THEOS_PACKAGE_SCHEME = $(if $(RF),rootful,rootless)
export THEOS_PACKAGE_INSTALL_PREFIX = /var/jb

ifneq ($(THEOS_PACKAGE_SCHEME),rootless)
# if building for rootful, meaning iOS <= 14 or a checkm8 jb. Find some Xcode 11 toolchain so it's the "old" ABI.
XCODE11PATHS := $(wildcard /Applications/Xcode*11*app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin $(THEOS)/toolchain/Xcode*11*.xctoolchain/usr/bin)
ifneq ($(XCODE11PATHS),)
export PREFIX := $(lastword $(XCODE11PATHS))/
endif
# reset the jb root prefix for rootful
export THEOS_PACKAGE_INSTALL_PREFIX =
endif

include $(THEOS)/makefiles/common.mk

BUNDLE_NAME = IPonCC
$(BUNDLE_NAME)_BUNDLE_EXTENSION = bundle
$(BUNDLE_NAME)_CFLAGS += -fobjc-arc -fvisibility=hidden -I$(THEOS_PROJECT_DIR)/headers
$(BUNDLE_NAME)_FILES = IPonCC.m
$(BUNDLE_NAME)_INSTALL_PATH = $(THEOS_PACKAGE_INSTALL_PREFIX)/Library/ControlCenter/Bundles
# ^ this prefix isn't necessary with newer Theos but for older ones it's easier if the path already includes it.
$(BUNDLE_NAME)_FRAMEWORKS = UIKit
$(BUNDLE_NAME)_PRIVATE_FRAMEWORKS = ControlCenterUIKit
# ^ ie. /opt/theos/sdks/iPhoneOS11.0.sdk/System/Library/PrivateFrameworks/ControlCenterUIKit.framework/ControlCenterUIKit.tbd

ifeq ($(THEOS_PACKAGE_SCHEME),rootless)
ifneq ($(THEOS_PACKAGE_ARCH),iphoneos-arm64)
# so this must be some older (pre 2023-03-26) Theos, need to mangle things a bit when packaging. 

# control file which we make in before-stage::
_THEOS_DEB_PACKAGE_CONTROL_PATH = "$(THEOS_PROJECT_DIR)/control-rl"

# for the filename and for the arch in before-stage::                                                                                                                                                
THEOS_PACKAGE_ARCH = iphoneos-arm64

before-stage::
	$(ECHO_NOTHING)sed -e 's/Architecture: iphoneos-arm$$/Architecture: $(THEOS_PACKAGE_ARCH)/' "$(THEOS_PROJECT_DIR)/control" > "$(THEOS_PROJECT_DIR)/control-rl"$(ECHO_END)

before-package::
# some backporting from makefiles/package/deb.mk post 2023-03-26 internal-package::
	$(ECHO_NOTHING)mkdir -p "$(THEOS_STAGING_DIR)$(THEOS_PACKAGE_INSTALL_PREFIX)"$(ECHO_END)
	$(ECHO_NOTHING)rsync -a "$(THEOS_STAGING_DIR)/" "$(THEOS_STAGING_DIR)$(THEOS_PACKAGE_INSTALL_PREFIX)" --exclude "DEBIAN" --exclude "$(THEOS_PACKAGE_INSTALL_PREFIX)" $(_THEOS_RSYNC_EXCLUDE_COMMANDLINE) $(ECHO_END)
	$(ECHO_NOTHING)find "$(THEOS_STAGING_DIR)" -mindepth 1 -maxdepth 1 ! -name DEBIAN ! -name "var" -exec rm -rf {} \;$(ECHO_END)
	$(ECHO_NOTHING)rmdir "$(THEOS_STAGING_DIR)$(THEOS_PACKAGE_INSTALL_PREFIX)/var" >/dev/null || true$(ECHO_END)

endif
endif

internal-stage::
	$(ECHO_NOTHING)mkdir -p $(THEOS_STAGING_DIR)$(THEOS_PACKAGE_INSTALL_PREFIX)/Library/ControlCenter/Bundles/$(BUNDLE_NAME)x3.bundle && { pushd $(THEOS_STAGING_DIR)$(THEOS_PACKAGE_INSTALL_PREFIX)/Library/ControlCenter/Bundles/$(BUNDLE_NAME)x3.bundle >/dev/null; cp $(THEOS_PROJECT_DIR)/IPonCCx3/Info.plist .; ln -s ../$(BUNDLE_NAME).bundle/$(BUNDLE_NAME) .; ln -s ../$(BUNDLE_NAME).bundle/SettingsIcon@2x.png .; ln -s ../$(BUNDLE_NAME).bundle/SettingsIcon@3x.png .; popd >/dev/null; }$(ECHO_END)

after-package::
	$(ECHO_NOTHING)rm -f "$(THEOS_PROJECT_DIR)/control-rl" >/dev/null || true$(ECHO_END)

include $(THEOS_MAKE_PATH)/bundle.mk
