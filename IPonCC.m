#import "IPonCC.h"

#include <stdlib.h>
#include <string.h>
#include <ifaddrs.h>
#include <arpa/inet.h>

/* START IOCLT TEST STUFF */
#include <sys/ioctl.h> /* this also includes sockio.h, SIOCGIFAGENTIDS etc */
#include <net/if_dl.h> /* for getting MAC addresses */

// from xnu-4570.1.46/bsd/net/if.h (about iOS 11.0 beta something)
// (= at least until xnu-7195.101.1 = macOS 11.3 = about iOS 14.5)

#ifndef IF_NAMESIZE
#define	IF_NAMESIZE	16
#endif

#ifndef IFNAMSIZ
#define	IFNAMSIZ	IF_NAMESIZE
#endif

/*
 * Structure for SIOCGIFAGENTIDS
 */
struct if_agentidsreq {
    char            ifar_name[IFNAMSIZ];    /* interface name */
    u_int32_t       ifar_count;             /* number of agent UUIDs */
    uuid_t          *ifar_uuids;            /* array of agent UUIDs */
};

// I'm using iOS 11 SDK for this, some stuff is stripped from the bsd headers there for some reason...

// additions to sys/sockio.h
// from xnu-4570.1.46/bsd/sys/sockio.h
#define SIOCGIFAGENTIDS _IOWR('i', 167, struct if_agentidsreq) /* Get netagent ids */
#define SIOCGIFAGENTDATA        _IOWR('i', 168, struct netagent_req) /* Get netagent data */

// from xnu-4903.221.2/bsd/net/network_agent.h
#define NETAGENT_DOMAINSIZE             32
#define NETAGENT_TYPESIZE               32
#define NETAGENT_DESCSIZE               128
// To be used with SIOCGAGENTDATA
struct netagent_req {
    uuid_t          netagent_uuid;
    char            netagent_domain[NETAGENT_DOMAINSIZE];
    char            netagent_type[NETAGENT_TYPESIZE];
    char            netagent_desc[NETAGENT_DESCSIZE];
    u_int32_t       netagent_flags;
    u_int32_t       netagent_data_size;
    u_int8_t        *netagent_data;
};
/* END IOCLT TEST STUFF */

/* START logger (all calls of bhLogIoC() are commented out for production builds) *
// https://github.com/Muirey03/RemoteLog
#include <RemoteLog.h>
#define DEBUG 1
NSDateFormatter* bhLogTSformat;

static void bhLogIoC(NSString *format, ...){
	if ( !bhLogTSformat ){
		bhLogTSformat = [[NSDateFormatter alloc] init];
		[bhLogTSformat setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSSS"];
	}
	va_list args;
	va_start(args, format);
	//RLogv([NSString stringWithFormat:@"%.4f u.blanxd.iponcc %@\n", ([[NSDate date] timeIntervalSince1970] * 1000.0)/1000, format], args);
//// TODO: device name
	RLogv([NSString stringWithFormat:@"%@ u.blanxd.iponcc %@\n", [bhLogTSformat stringFromDate:[NSDate date]], format], args);
	va_end(args);
}
* END logger */


@implementation IPonCcViewController {
	BOOL amExpanded;
	BOOL amTransitioning;
	BOOL amDisappearing;
	
	NSDictionary *ifsAndAdrs;
	NSMutableDictionary *nameToBsd;
	NSMutableDictionary *macs;
	NSArray *sectionNames;
	
	UILabel *txtLabel;
	UITableView *tableView;
	UIView *botButBar;
	UILabel *countLabel;
	UISegmentedControl *showAllButton;
	
	NSIndexPath *ipCopied;
	NSTimer *copyTimer;
	NSTimer *reloader;
	
	BOOL showAll;
	BOOL wideOption;
	
	double radiusCollapsed;
	double radiusExpanded;
	
	int cnt4;
	int cnt6;
	
	NSArray *orderHumanStr;
	NSArray *orderDevNames;
}

@synthesize toggleModule;
#ifndef IPonCCheaders
@synthesize providesOwnPlatter;
#endif

-(IPonCcViewController *) init {
	self = [super init];

//bhLogIoC(@"init"); //// debug

	// view status
	amExpanded = NO;
	amTransitioning = NO;
	amDisappearing = NO;

	// init
	ifsAndAdrs = @{};
	nameToBsd = [NSMutableDictionary dictionary];
	macs = [NSMutableDictionary dictionary];
	sectionNames = @[];
	ipCopied = nil;
	reloader = nil;
	showAll = NO;
	wideOption = NO;

	radiusCollapsed = -1;
	radiusExpanded = -1;

	cnt4=0;
	cnt6=0;

	// v.1.4.5 : we have 11 possible options now (5* Cellular, show them separately now) plus possible "Cellular 0" if it isn't the main address (dunno if that ever happens)
	orderHumanStr = @[@"Cellular",@"Cellular 0",@"Cellular 2",@"Cellular 3",@"Cellular 4",@"Cellular 5",@"WiFi",@"Ethernet",@"VPN",@"USB",@"Hotspot"];
	// v.1.5 : let's order the bsd names a bit also
	orderDevNames = @[@"pd",@"en",@"br",@"ip",@"ut"];

	return self;
} // -(IPonCcViewController *) init {

-(instancetype)initWithToggleModule:(CCUIToggleModule *)givenToggleModule {
	self = [self init];
	self.toggleModule = givenToggleModule;
	return self;
}

-(void)viewWillAppear:(BOOL)animated {
//bhLogIoC(@"viewWillAppear"); //// debug
	[super viewWillAppear:animated];
	amDisappearing = NO;
	[self ipLayout];
}
- (void)viewWillDisappear:(BOOL)animated {
//bhLogIoC(@"viewWillDisappear"); //// debug
	amDisappearing = YES; // after this gets called, it makes us transition to collapsed view so need to track it
	[self stopReloader];
}
- (void)viewDidAppear:(BOOL)animated {
//bhLogIoC(@"viewDidAppear"); //// debug
	amDisappearing = NO;
	// need to restart reloader for wideOption if coming from 3rd party expanded view
	if ( wideOption && !amExpanded ){
		[self startReloader];
	}

}

- (void)viewDidLoad {
	[super viewDidLoad];

	// ideas from https://github.com/hamzasood/CustomControlCenterModules/blob/master/TestCustomUIModule/src/HSCustomUIModuleContentViewController.m
	_preferredExpandedContentWidth = UIScreen.mainScreen.bounds.size.width>400 ? 400 : UIScreen.mainScreen.bounds.size.width - 40;
	CGFloat regular = _preferredExpandedContentWidth * 1.6;
	CGFloat maxHeight = UIScreen.mainScreen.bounds.size.height - 40;
	if ( regular > maxHeight ){
		_preferredExpandedContentHeight =  maxHeight;
	}
	_preferredExpandedContentHeight =  regular;

}

#pragma mark Expand/Collapse


// one-touch expansion (for the IP button) on iOS 11-12 (maybe 13). 3D/long touch doesn't trigger this.
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//bhLogIoC(@"touchesEnded"); //// debug

	if ( !amExpanded && !amTransitioning 
		&& touches.count==1 && event.type==UIEventTypeTouches 
		&& self.toggleModule != nil && [self.toggleModule respondsToSelector:@selector(contentModuleContext)]
	){
//bhLogIoC(@"touchesEnded expanding with contentModuleContext>requestExpandModule"); //// debug
		[[self.toggleModule contentModuleContext] requestExpandModule];
	}

	[super touchesEnded:touches withEvent:event];
}

// one-touch expansion (for the IP button) since iOS 14. Looks like this actually also works on iOS 13 already???? is this some hidden thing there...
// not in https://developer.limneos.net/index.php?ios=13.1.3&framework=ControlCenterUIKit.framework&header=CCUIContentModuleContentViewController.h
// but works on 13.4.1
// 3D/long touch doesn't trigger this.
-(BOOL)shouldExpandModuleOnTouch:(id)arg1 {
//bhLogIoC(@"shouldExpandModuleOnTouch < %@ > %d", arg1, !wideOption); //// debug
	return !wideOption; // else this disables our copying functionality on the 3x1 module.
}

// this also gets called when the screen locks up while the Expanded module is on screen
// ie. it transitions to collapsed mode before the screen locks
// so let's keep track of viewWillDisappear/viewDidAppear with (BOOL)amDisappearing
- (void)didTransitionToExpandedContentMode:(BOOL)didTransition {
//bhLogIoC(@"didTransitionToExpandedContentMode %@", didTransition?@"YES":@"NO"); //// debug
	amExpanded = didTransition;
	amTransitioning = NO;
	if ( !amDisappearing ){
		[self ipLayout]; // reload it here
	}
}

// need to clear it before it transitions, else the previous content might flicker for a moment there
// this also gets called when the screen locks up while the Expanded module is on screen
- (void)willTransitionToExpandedContentMode:(BOOL)willTransition {
//bhLogIoC(@"willTransitionToExpandedContentMode < %d", willTransition); //// debug
	[copyTimer invalidate];
	ipCopied = nil;
	amExpanded = willTransition;
	amTransitioning = YES;
	[self clearTheLayout];
	[macs removeObjectForKey:@"en0"]; // WiFi mac changes when toggling "Private Address". So if the user is fast it may happen that it changes while we're loaded. Don't want to read it every second, let's refresh it here at least.
}


#pragma mark IP Table (and IP button) layout

-(void)ipLayout {

	[self clearTheLayout];
	if ( amTransitioning ) return;

//bhLogIoC(@"ipLayout %f %f", self.view.bounds.size.width, self.view.bounds.size.height); //// debug

	if ( !amExpanded && self.view.bounds.size.width > self.view.bounds.size.height )
		wideOption = YES;

//bhLogIoC(@"ipLayout wideOption:%d amTransitioning:%d amExpanded:%d", wideOption, amTransitioning, amExpanded); //// debug

	if (
		( amExpanded && radiusExpanded == -1 )
		|| ( !amExpanded && radiusCollapsed == -1 )
	){
/**
iOS 11 ip5s: 34 / 17
iOS 12 ip6s: 38 / 19
iOS 13 ipX:  38 / 19
iOS 14 ip8: 38 / 19
iOS 14 ipXS: 38 / 19
iOS 14 ip12p: 40 / 20
**/
		if ( [self.parentViewController respondsToSelector: amExpanded ? @selector(_continuousCornerRadiusForExpandedState) : @selector(_continuousCornerRadiusForCompactState)] ){
			if (amExpanded)
				radiusExpanded = [(CCUIContentModuleContainerViewController*)self.parentViewController _continuousCornerRadiusForExpandedState];
			else
				radiusCollapsed = [(CCUIContentModuleContainerViewController*)self.parentViewController _continuousCornerRadiusForCompactState];
		} else if ( [(CCUIContentModuleContainerViewController*)self.parentViewController respondsToSelector:@selector(contentContainerView)]
				&& [[(CCUIContentModuleContainerViewController*)self.parentViewController contentContainerView] respondsToSelector:@selector(moduleMaterialView)]
				&& [[[(CCUIContentModuleContainerViewController*)self.parentViewController contentContainerView] moduleMaterialView] respondsToSelector:@selector(_continuousCornerRadius)]
			){
			if (amExpanded)
				radiusExpanded = [[[(CCUIContentModuleContainerViewController*)self.parentViewController contentContainerView] moduleMaterialView] _continuousCornerRadius];
			else
				radiusCollapsed = [[[(CCUIContentModuleContainerViewController*)self.parentViewController contentContainerView] moduleMaterialView] _continuousCornerRadius];
		} else if (amExpanded){
			radiusExpanded = 34;
		} else {
			radiusCollapsed = 17;
		}
	}

	self.view.clipsToBounds = YES;
	self.view.layer.cornerRadius = amExpanded ? radiusExpanded : radiusCollapsed;

	if ( amExpanded || wideOption ){
		[self ipAddressList];

		tableView = [[UITableView alloc] initWithFrame:(CGRect){.origin = CGPointMake(0,0), .size = self.view.bounds.size} style: amExpanded ? UITableViewStyleGrouped : UITableViewStylePlain];
		tableView.delegate = self;
		tableView.dataSource = self;
		tableView.showsVerticalScrollIndicator = NO;
		tableView.layer.masksToBounds = YES;
		[self wideOptionRowHeight];
		tableView.backgroundColor = [UIColor clearColor];
		tableView.sectionHeaderHeight = 32;

		[self.view addSubview:tableView];

		[self startReloader];

		if ( amExpanded ){
			if ( !botButBar ){
				botButBar = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height-32, self.view.bounds.size.width, 32)];
				botButBar.clipsToBounds = YES;
				botButBar.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1];
			}
			[self.view insertSubview:botButBar aboveSubview:tableView];

			if ( !countLabel ){
				countLabel = [[UILabel alloc] initWithFrame:CGRectMake(radiusExpanded/2, 0, self.view.bounds.size.width/2, 32)];
				countLabel.textColor = [UIColor whiteColor];
				countLabel.textAlignment = NSTextAlignmentLeft;
				[countLabel setFont: [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote]];
				countLabel.numberOfLines = 1;
				[botButBar addSubview:countLabel];
			}
			countLabel.text = [NSString stringWithFormat:@"v4:%d v6:%d", cnt4, cnt6];

			if ([botButBar.subviews.lastObject isKindOfClass:[UISegmentedControl class]]) {
				showAllButton = (UISegmentedControl *)[botButBar.subviews lastObject];
			}
			if ( showAllButton != nil ){
				[showAllButton setSelectedSegmentIndex: showAll ? 1 : 0];
			} else {
				showAllButton = [[UISegmentedControl alloc] initWithItems:@[@"Human Readable",@"Internal"]];
				showAllButton.tintColor = [UIColor whiteColor];
				showAllButton.apportionsSegmentWidthsByContent = YES;
				[showAllButton setSelectedSegmentIndex: showAll ? 1 : 0];
				[showAllButton addTarget:self action:@selector(showHRorInternal:) forControlEvents:UIControlEventValueChanged];

				showAllButton.translatesAutoresizingMaskIntoConstraints = NO;
				[botButBar addSubview:showAllButton];
				[showAllButton.trailingAnchor constraintEqualToAnchor:botButBar.layoutMarginsGuide.trailingAnchor].active = YES;
				[showAllButton.centerYAnchor constraintEqualToAnchor:botButBar.layoutMarginsGuide.centerYAnchor].active = YES; // at least iOS 11 needs this as well else it might be positioned off screen...
			}
			
		} else if (botButBar){
			[botButBar removeFromSuperview];
		}

	} else {
		[self stopReloader];

		if ( !txtLabel ){

			CGSize viewSize;
			if ( self.viewLoaded ){ // could it not exist if we're creting this in viewWillAppear() ?
				viewSize = self.view.bounds.size;
			} else {
				viewSize = self.preferredContentSize;
			}
			//   5s: 66*66
			// 6-Xs: 69*69
			//  12p: 72*72

			txtLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, viewSize.width-30, viewSize.height-30)];
			txtLabel.center = CGPointMake(viewSize.width/2, viewSize.height/2);
			txtLabel.textAlignment = NSTextAlignmentCenter;
			txtLabel.textColor = [UIColor whiteColor];
			txtLabel.attributedText = [[NSAttributedString alloc] initWithString:@"IP" attributes:@{NSFontAttributeName:[UIFont preferredFontForTextStyle:UIFontTextStyleTitle1]}];
			txtLabel.numberOfLines = 1;
		}

		[self.view addSubview:txtLabel];
	}

} // -(void)ipLayout {

// need to keep it empty while Expanding/Collapsing
-(void)clearTheLayout {
	self.view.layer.cornerRadius = radiusCollapsed;
	[[self.view subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
	tableView = nil;
}

- (IBAction)showHRorInternal:(UISegmentedControl *)SControl {
	if ( copyTimer.valid ) [copyTimer fire];
	[self stopReloader];
	showAll = SControl.selectedSegmentIndex > 0;
	[self reloadAddresses];
	[self startReloader];	
}

- (void)copyMac:(UITapGestureRecognizer *)sender {
	if ( ipCopied == nil ){ // one at a time.
		[UIPasteboard generalPasteboard].string = [(UILabel *)[sender view] text];
		ipCopied = [NSIndexPath indexPathForRow:-1 inSection:sender.view.tag];		
		UILabel *macLabel = (UILabel *)sender.view;
		macLabel.text = @"mac got copied";		
		copyTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer *timer){
			if ( ipCopied != nil ){
				NSInteger section = ipCopied.section;
				ipCopied = nil;
				UILabel *macLabel = (UILabel *)[[[tableView headerViewForSection:section] subviews] lastObject];
				if ( macLabel != nil )
					macLabel.text = [macs objectForKey: showAll ? [sectionNames objectAtIndex:section] : [nameToBsd objectForKey:[sectionNames objectAtIndex:section]] ];
			}
		}];
	}
}

#pragma mark TableView helpers

// helper for saving current section names (sorted)
- (void)saveSectionNames:(NSDictionary *)addrs {

	if ( showAll && amExpanded ){
		// v.1.5: order those as well, a bit, by type
		sectionNames = [[addrs allKeys] sortedArrayWithOptions:NSSortStable usingComparator:^(NSString *obj1, NSString *obj2) {

			NSUInteger ind1 = [orderDevNames indexOfObject:[obj1 substringToIndex:2]];
			NSUInteger ind2 = [orderDevNames indexOfObject:[obj2 substringToIndex:2]];

			if ( ind1==ind2 ){ // either both are NSNotFound or they are of the same type
				return [obj1 compare:obj2];
			}
			
			return ind1 < ind2
				? (NSComparisonResult)NSOrderedAscending
				: (NSComparisonResult)NSOrderedDescending;
			
		}];
		return;
	}

	// v.1.4.5 : we have 11 possible options now (5* Cellular, show them separately now) plus possible "Cellular 0" if it isn't the main address (dunno if that ever happens)
	NSMutableArray *current = [NSMutableArray arrayWithArray:orderHumanStr];
	for (int j = 0; j < orderHumanStr.count; j++){
		if ( [addrs objectForKey:(NSString *)[orderHumanStr objectAtIndex:j]]==nil ){
			[current removeObject:[orderHumanStr objectAtIndex:j]];
		}
	}
	if ( current.count < 1 ){
		sectionNames = @[@"(none)"];
	} else {
		sectionNames = [NSArray arrayWithArray:current];
	}
}

// helper for wideOption rowHeight
- (void)wideOptionRowHeight {
	if ( amExpanded ){
		tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
		tableView.separatorColor = [UIColor systemGrayColor]; // systemGrayColor since iOS7 // [UIColor whiteColor];
	} else
	if ( wideOption ){
		tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
		tableView.separatorColor = [UIColor clearColor];
		int count = ifsAndAdrs.count;
		if ( count < 1 ) count = 1;
		if ( count > 3 ) count = 3;
		float tbEdge = count==1 ? 6 : (count==2 ? 4 : 2);
		tableView.contentInset = UIEdgeInsetsMake(tbEdge,0,tbEdge,0); // top,left,bottom,right : pt (not px luckily)
		tableView.rowHeight = (self.view.bounds.size.height-(2*tbEdge)) / count;
	}
}

// helper for wideOption row mapping
- (NSIndexPath *)realIndexPath:(NSIndexPath *)indexPath {
	NSIndexPath * useIndexPath = indexPath;

	if (wideOption && !amExpanded){
		// so they're all in one section, meaning .section is 0 and .row is ...
		NSUInteger foundSec = 0;
		NSUInteger foundRow = 0;
		int i = 0;

		BOOL br = NO;
		for (NSString* key in ifsAndAdrs) {
			NSArray* vals = (NSArray *)[ifsAndAdrs objectForKey:key];
			foundRow = 0;
			for (int j = 0; j < [vals count]; j++){
				if ( i == indexPath.row ){
					br = YES;
					break;
				}
				foundRow++;
				i++;
			}
			if ( br ){
				break;
			}
			foundSec++;
		}
		useIndexPath = [NSIndexPath indexPathForRow:foundRow inSection:foundSec];
//bhLogIoC(@"given sec:%ld row:%ld found sec:%lu row:%lu", indexPath.section, indexPath.row, foundSec, foundRow); //// debug
	}
	return useIndexPath;
} // - (NSIndexPath *)realIndexPath:(NSIndexPath *)indexPath {


#pragma mark TableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)table {
	if ( [ifsAndAdrs count]==0 // (none)
		|| (wideOption && !amExpanded) // all ipv4 addrs in one section
		){
		return 1;
	}
	return sectionNames.count;
}

// makes the table use a label, but this actually gets overriden by willDisplayHeaderView in the delegate part
- (NSString *)tableView:(UITableView *)table titleForHeaderInSection:(NSInteger)section {
	if ( !amExpanded ){
		return nil;
	}
	return @" "; // it gets overridenn anyway but here we need a non-empty string else it thinks there needs to no header
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {

	if ( wideOption && !amExpanded ){
		int count = 0;
		for (NSString *key in ifsAndAdrs) {
			NSArray* vals = (NSArray*)ifsAndAdrs[key];
			count += vals.count;
		}
		return count > 0 ? (count>3?3:count) : 1; // max 3 rows for 3x1 view
	}
	if ( ifsAndAdrs.count < 1 ){
		return 0;
	}

	return [(NSArray *)[ifsAndAdrs objectForKey:[sectionNames objectAtIndex:section]] count];    
}
- (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	NSIndexPath *useIndexPath = [self realIndexPath:indexPath];
//bhLogIoC(@"cellForRowAtIndexPath: %@ %@", indexPath, useIndexPath); //// debug

	UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:@"cell"];
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
		cell.backgroundColor = [UIColor clearColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES; //// TODO: DEPREC since iOS 14, but contentConfiguration also only since iOS 14. Ok still present in 16.
		cell.textLabel.minimumScaleFactor = 0.2;
		cell.textLabel.textColor = [UIColor whiteColor]; // lightTextColor ?
	}
	NSString *sectionName = [sectionNames objectAtIndex:useIndexPath.section];

	cell.textLabel.text = ( wideOption && !amExpanded ) //// TODO: DEPREC since iOS 14, but contentConfiguration also only since iOS 14. Ok still present in 16.
		// small table
		? ( ifsAndAdrs.count<1
			? @"(zero IP addresses)"
			: [NSString stringWithFormat:@"%@: %@", 
				[sectionName isEqualToString:@"Cellular"] 
					? @"Cell" 
					: [sectionName isEqualToString:@"Ethernet"] 
						? @"Ether"
						: sectionName,
				ipCopied==indexPath
					? @"address got copied"
					: (NSString *)[(NSArray *)[ifsAndAdrs objectForKey:sectionName] objectAtIndex:useIndexPath.row]
				]
		)
		// expanded table
		: ( ipCopied==indexPath
			? @"this address got copied"
			: (NSString *)[(NSArray *)[ifsAndAdrs objectForKey:sectionName] objectAtIndex:indexPath.row]
		);

	return cell;
} // - (UITableViewCell *)tableView:(UITableView *)table cellForRowAtIndexPath:(NSIndexPath *)indexPath {


#pragma mark TableView delegate

- (void)tableView:(UITableView *)table willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {

//// TODO?: might put a Refresh button to the right of the 1st header, and remove the ticker. With a chekbox, or something. Or on the bottom bar.

	UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
	header.contentView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];

//bhLogIoC(@"willDisplayHeaderView section:%lu", section); //// debug

	if ( !wideOption || amExpanded ){
		[header.textLabel setFont: [UIFont preferredFontForTextStyle:UIFontTextStyleBody]]; //// TODO: DEPREC since iOS 14, but contentConfiguration also only since iOS 14. Ok still present in 16.
		[header.textLabel setTextColor:[UIColor whiteColor]];
		NSString *sectionName = [sectionNames objectAtIndex:section];
		header.textLabel.text = [NSString stringWithFormat:@"%@%@", 
			sectionName, 
			( showAll || cnt4+cnt6==0 )
				? @"" 
				: [NSString stringWithFormat:@" (%@)", [nameToBsd objectForKey:sectionName]]
			];

		NSString *mac;
		if ( ipCopied != nil && ipCopied.row == -1 && ipCopied.section == section )
			mac = @"mac got copied";
		else {
			mac = [macs objectForKey: showAll ? sectionName : [nameToBsd objectForKey:sectionName] ];
			if ( mac == nil 
				|| [mac compare:@"00:00:00:00" options:NSLiteralSearch range:NSMakeRange(3,11)] == NSOrderedSame
			) mac = @"";
		}

		UILabel *macLabel;
		if ([view.subviews.lastObject isKindOfClass:[UILabel class]]) {
			macLabel = (UILabel *)[view.subviews lastObject];
		}
		if ( macLabel != nil ){
			macLabel.text = mac;
			macLabel.tag = section;
		} else {
			macLabel = [UILabel new];
			macLabel.textColor = [UIColor whiteColor];
			macLabel.text = mac;
			macLabel.tag = section;

			UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(copyMac:)];
			tapGestureRecognizer.numberOfTapsRequired = 1;
			[macLabel addGestureRecognizer:tapGestureRecognizer];
			macLabel.userInteractionEnabled = YES;

			macLabel.translatesAutoresizingMaskIntoConstraints = NO;
			[view addSubview:macLabel];
			[macLabel.trailingAnchor constraintEqualToAnchor:view.layoutMarginsGuide.trailingAnchor].active = YES;
			[macLabel.centerYAnchor constraintEqualToAnchor:view.layoutMarginsGuide.centerYAnchor].active = YES; // at least iOS 11 needs this as well else it might be positioned off screen...
//bhLogIoC(@"willDisplayHeaderView section: mac %@ into %@", macLabel, view); //// debug
		}

	} // if ( !wideOption || amExpanded ){
} // - (void)tableView:(UITableView *)table willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {

// without defining this it would display a footer for each section, despite heightForFooterInSection returning 0.
- (UIView *)tableView:(UITableView *)table viewForFooterInSection:(NSInteger)section{
//bhLogIoC(@"viewForFooterInSection for section %ld", section); //// debug
	return nil;
}

- (CGFloat)tableView:(UITableView *)table heightForFooterInSection:(NSInteger)section {
//bhLogIoC(@"heightForFooterInSection for section %ld", section); //// debug
	return ( amExpanded && ifsAndAdrs.count-1 == section )
		? 32 // some extra scrolling space in the end so the bottom bar wouldn't cover the last address row
		: 0;
}

- (NSIndexPath *)tableView:(UITableView *)table willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	return ( ipCopied == nil // one at a time
			&& ifsAndAdrs.count > 0 // there are some
			)
		? indexPath
		: nil;
}

// copy the address
- (void)tableView:(UITableView *)tablView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tablView deselectRowAtIndexPath:indexPath animated:ipCopied==nil];
	if ( ipCopied == nil ){ // one at a time. ?redundant here?
		NSString *txt = [tablView cellForRowAtIndexPath:indexPath].textLabel.text; //// TODO: DEPREC since iOS 14, but contentConfiguration also only since iOS 14. Ok still present in 16.
		NSRange split = [txt rangeOfString:@" "];
		if ( split.location != NSNotFound && split.location > 0 ){
			if ( !amExpanded ){
				txt = [txt substringFromIndex:split.location+1];
				split = [txt rangeOfString:@" "];
				if ( split.location == NSNotFound || split.location == 0 )
					split = NSMakeRange(txt.length,0);
			}
			txt = [txt substringToIndex:split.location];
		}
		[UIPasteboard generalPasteboard].string = txt;
		ipCopied = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
//bhLogIoC(@"copy %ld %ld %@", indexPath.section, indexPath.row, txt); //// debug
		[tablView reloadRowsAtIndexPaths:@[ ipCopied ] withRowAnimation:UITableViewRowAnimationFade];
		copyTimer = [NSTimer scheduledTimerWithTimeInterval:1.5 repeats:NO block:^(NSTimer *timer){
			if ( ipCopied != nil ){
//bhLogIoC(@"scheduledTimerWithTimeInterval after copy"); //// debug
				NSIndexPath *rlIndex = ipCopied;
				ipCopied = nil;
				[tablView reloadRowsAtIndexPaths:@[ rlIndex ] withRowAnimation:UITableViewRowAnimationFade];
			}
		}];
	}
}

#pragma mark Ticker
- (void)reloadAddresses {
	if ( !wideOption && (amTransitioning || !amExpanded) ){
//bhLogIoC(@"reloadAddresses return"); //// debug
		return;
	}

//bhLogIoC(@"%@ reloading", wideOption?@"x3":@""); //// debug

	NSDictionary *oldAddresses = [NSDictionary dictionaryWithDictionary:ifsAndAdrs]; // does the retain count on the objects then decrease after this func...?
	NSArray *oldSectionNames = [NSArray arrayWithArray:sectionNames]; // does the retain count on the objects then decrease after this func...?
	int old4 = cnt4;
	int old6 = cnt6;
	[self ipAddressList];

	if ( amExpanded && (old4 != cnt4 || old6 != cnt6) )
		countLabel.text = [NSString stringWithFormat:@"v4:%d v6:%d", cnt4, cnt6];

	if ( ![oldSectionNames isEqualToArray:sectionNames] ){
//bhLogIoC(@"%@ reloading old:%@ new:%@", wideOption?@"x3":@"", oldSectionNames, sectionNames); //// debug
		if ( !amExpanded )
			[self wideOptionRowHeight];
		[tableView reloadData];
		return;
	}
	if ( ifsAndAdrs.count==0 ) return;
	int section = 0;
	for( NSString *ifn in oldSectionNames ){
		if ( ![(NSArray*)[oldAddresses objectForKey:ifn] isEqualToArray:(NSArray*)[ifsAndAdrs objectForKey:ifn]] ){
//bhLogIoC(@"%@ reloading %@ %lu %@", wideOption?@"x3":@"", ifn, section, sectionsToReload); //// debug
			[tableView reloadSections:[NSMutableIndexSet indexSetWithIndex:amExpanded?section:0] withRowAnimation:UITableViewRowAnimationAutomatic];
			if (!amExpanded) break;
		}
		section++;
	}

} // - (void)reloadAddresses {

// reloader helpers
- (void)startReloader {
	if (reloader == nil || !reloader.valid){
//bhLogIoC(@"startReloader"); //// debug
		reloader = [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer *timer){
			[self reloadAddresses];
		}];
	}
//else bhLogIoC(@"startReloader unnecessary"); //// debug
}
- (void)stopReloader {
	if ( reloader != nil && reloader.valid ){
		[reloader invalidate];
//bhLogIoC(@"stopReloader"); //// debug
	}
//else bhLogIoC(@"stopReloader no reloader found"); //// debug
}


#pragma mark The Workhorse
// I started form here https://stackoverflow.com/questions/6807788/how-to-get-ip-address-of-iphone-programmatically
- (void)ipAddressList {

	// I want to show the ipv4 addresses 1st, so populate 2 separate dicts and combine later
	NSMutableDictionary *adrs4 = [[NSMutableDictionary alloc] init];
	NSMutableDictionary *adrs6 = [[NSMutableDictionary alloc] init];
	
	cnt4=0;
	cnt6=0;
	
	struct ifaddrs *interfaces = NULL;
	struct ifaddrs *temp_addr = NULL;
	int success = 0;
	// retrieve the current interfaces - returns 0 on success
	success = getifaddrs(&interfaces);
	if (success == 0) {
		const char *ifname; // v.1.5: let's spare on the objc processing, does it even make a difference...?
		NSString *ifName;
		NSString *bsdIfName;
		NSString *ifType;

		// Loop through linked list of interfaces
		temp_addr = interfaces;
		while(temp_addr != NULL) {
			if((temp_addr->ifa_addr)){
				if ( temp_addr->ifa_addr->sa_family == AF_LINK ){
					bsdIfName = [NSString stringWithUTF8String:(temp_addr)->ifa_name];
					if ( [macs objectForKey:bsdIfName] == nil ){
						// in sandboxed apps this doesn't give us the macs, but in SpringBoard it looks like it does
						unsigned char *ptr;
						ptr = (unsigned char *)LLADDR((struct sockaddr_dl *)(temp_addr)->ifa_addr);
						[macs setObject:[NSString stringWithFormat:@"%02x:%02x:%02x:%02x:%02x:%02x", *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)] forKey:bsdIfName];
//bhLogIoC(@"ipAddressList %s mac: %@", (temp_addr)->ifa_name, [macs objectForKey:bsdIfName]); //// debug
					}
				} else 
				if ( (temp_addr->ifa_addr->sa_family == AF_INET) || (temp_addr->ifa_addr->sa_family == AF_INET6) ){

					ifname = temp_addr->ifa_name;
					ifName = [NSString stringWithUTF8String:ifname];
					bsdIfName = ifName;

					ifType = @"";

// Some observations in Estonia:
// ap1: Personal Hotspot..?
// awdl0: Apple Wireless Direct Link
// bridge100: Hotspot > clients
// en0: WiFi
// en1: USB link
// en2: USB link
// en3: Ethernet
// en4: Ethernet
// ipsec0: 4g voice (or VPN)
// ipsec1: 4g voice
// ipsec2: 4g voice if 0 is VPN (or VPN)
// ipsec3: VPN
// ipsec4: VPN
// pdp_ip0: cell
// pdp_ip1: 4g voice
// pdp_ip2: cell (for hotspot)
// pdp_ip3: ?
// pdp_ip4: ?
// utun0: ....members.btmm.icloud.com (Back To My Mac)
// utun1: VPN/???
// utun2: VPN
// XHC: USB?
// OHC: USB?
// EHC: USB?

					if ( !showAll || !amExpanded ){

/* START IOCLT TEST STUFF */

						int found = 0;
						int s;
						s = socket(temp_addr->ifa_addr->sa_family, SOCK_DGRAM, 0);
						if ( s != -1 ){

						struct if_agentidsreq ifar;
						memset(&ifar, 0, sizeof(ifar));
						//errno = 0; // for debug logging only

						strlcpy(ifar.ifar_name, temp_addr->ifa_name, sizeof(ifar.ifar_name));
						if (ioctl(s, SIOCGIFAGENTIDS, &ifar) != -1) {
							if (ifar.ifar_count != 0) {
								ifar.ifar_uuids = calloc(ifar.ifar_count, sizeof(uuid_t));
								if (ifar.ifar_uuids != NULL) {
									if (ioctl(s, SIOCGIFAGENTIDS, &ifar) != 1) {
										for (int agent_i = 0; agent_i < ifar.ifar_count; agent_i++) {
											struct netagent_req nar;
											memset(&nar, 0, sizeof(nar));

											uuid_copy(nar.netagent_uuid, ifar.ifar_uuids[agent_i]);

											if (ioctl(s, SIOCGIFAGENTDATA, &nar) != 1) {

												if ( strncmp(nar.netagent_domain,"Skywalk",7) != 0 ){ // domain:Skywalk type:FlowSwitch flags:0x83 desc:"MultiStack"

													if ( strncmp(nar.netagent_type,"VPN",3)==0 ){
														// done, no more figuring
//bhLogIoC(@"ipAddressList %@ 1stPass VPN", ifName); //// debug
														ifName = @"VPN";
														found = 1;
													} else if ( strncmp(nar.netagent_desc,"WiFi",4)==0 ){
														// for enX stuff basically I guess
//bhLogIoC(@"ipAddressList %@ 1stPass WiFi", ifName); //// debug
														ifName = @"WiFi";
														found = 1;
													} else if ( strncmp(nar.netagent_type,"Internet",8)==0 ){
														// for pdp_ipX stuff basically I guess
														// if I got this for one, need to make the others not be the main "Cell"
//bhLogIoC(@"ipAddressList %@ 1stPass type Internet", ifName); //// debug
														ifType = @"I";
													} else if ( strncmp(nar.netagent_type,"clientchannel",13)==0 ){
														// may happen for utunX, meaning it is no VPN, some xyz.members.btmm.icloud.com thing
//bhLogIoC(@"ipAddressList %@ 1stPass type clientchannel", ifName); //// debug
														ifType = @"c";
														//found = 1;
													}
												} // if ( strncmp(nar.netagent_domain,"Skywalk",7) != 0 ){

											} // if (ioctl(s, SIOCGIFAGENTDATA, &nar) != 1) {
											//else {
												//bhLogIoC(@"ERR %@: ioctl(s, SIOCGIFAGENTDATA, &nar) == 1", ifName); //// debug
											//}
											if ( found==1 ) break;

										} // for (int agent_i = 0; agent_i < ifar.ifar_count; agent_i++) {
									} //else {
										//bhLogIoC(@"ERR %@: ioctl(s, SIOCGIFAGENTIDS, &ifar) == 1", ifName); //// debug
									//}
									free(ifar.ifar_uuids);
								} //else {
								    //bhLogIoC(@"ERR %@: ifar.ifar_uuids == NULL", ifName); //// debug
								//}
							} //else {
								// this does happen, for lo0 and I've seen for some ipsecX as my ISP is using those for VoIP
								//  and for bridge100 (hotspot)
								//bhLogIoC(@"ERR %@: ifar.ifar_count == 0", ifName); //// debug
							//}
						}// else {
							//bhLogIoC(@"ERR %@: ioctl(s, SIOCGIFAGENTIDS, &ifar) == -1 errno:%d (%s)", ifName, errno, (errno==EBADF?"arg0 is not a valid descriptor":(errno==EINVAL?"Request or argp is not valid":(errno==ENOTTY?"arg0 is not associated with a character special device":"request does not apply to the kind of object that arg0 references"))) ); //// debug
						//}

						} // if ( s != -1 ){
						close(s);

/* END IOCLT TEST STUFF */

						if ( found==0 ){

							ifname = [ifName UTF8String]; // v.1.5: let's spare on the objc processing
							if ([ifType isEqualToString:@"I"]){
								ifName = @"Cellular";
								if ( [adrs4 objectForKey:@"Cellular"] != nil ){
									// meaning we're at some later Cellular, not the pdp_ip0, let's call the pdp_ip0 "Cellular 0"
									[adrs4 setObject:[adrs4 objectForKey:@"Cellular"] forKey:@"Cellular 0"];
									[adrs4 removeObjectForKey:@"Cellular"];
								}
								if ( [adrs6 objectForKey:@"Cellular"] != nil ){
									// meaning we're at some later Cellular, not the pdp_ip0, let's call the pdp_ip0 "Cellular 0"
									[adrs6 setObject:[adrs6 objectForKey:@"Cellular"] forKey:@"Cellular 0"];
									[adrs6 removeObjectForKey:@"Cellular"];
								}

							} else if ([ifType isEqualToString:@"c"]){
								ifName = @"";
							} else if ( strncmp(ifname,"en",2)==0 ){ // v.1.5
								if ( strncmp(ifname,"en0",3)==0 ){
									ifName = @"WiFi";
								} else
								// v.1.5: looks like since en1 and en2 are builtin, the USB connections happen over there
								if ( strncmp(ifname,"en1",3)==0 || strncmp(ifname,"en2",3)==0 ){
									ifName = @"USB";
								} else {
									ifName = @"Ethernet";
								}
							} else if ( strncmp(ifname,"pdp_ip",6)==0
								&& ( !wideOption || amExpanded || [ifName isEqualToString:@"pdp_ip0"] ) // for 3x1 only the 1st
								) {
								if ( [ifName isEqualToString:@"pdp_ip0"] ){
									ifName = @"Cellular";
								} else {
									NSString *cellIfNr = [ifName substringFromIndex:6];
//bhLogIoC(@"finalAddrs cell nr %@ %d", cellIfNr, [cellIfNr intValue]+1); //// debug
									ifName = [NSString stringWithFormat:@"Cellular %d", [cellIfNr intValue]+1];
								}
							} else if ( strncmp(ifname,"bridge",6)==0 || strncmp(ifname,"ap",2)==0 ){
								ifName = @"Hotspot";
							} else if ( 
								strncmp(ifname,"XHC",3)==0 
								|| strncmp(ifname,"OHC",3)==0 
								|| strncmp(ifname,"EHC",3)==0 
								) {
								ifName = @"USB";
							} else {
								ifName = @"";
							}

						} // if ( found==0 ){
					} // if ( !showAll || !amExpanded ){

					if (![ifName isEqualToString:@""]){
						[nameToBsd setObject:bsdIfName forKey:ifName];
						if (temp_addr->ifa_addr->sa_family == AF_INET){
							if ( [adrs4 objectForKey:ifName] == nil ){
								[adrs4 setObject:[[NSMutableArray alloc] init] forKey:ifName];
							}
							// Get NSString from C String
							char addr[INET_ADDRSTRLEN];
							inet_ntop(AF_INET, &((struct sockaddr_in*)temp_addr->ifa_addr)->sin_addr, addr, sizeof(addr));

							[[adrs4 objectForKey:ifName] addObject: [NSString stringWithFormat:@"%@ /%d",
								[NSString stringWithUTF8String:addr],
								mask4_as_int(((struct sockaddr_in*)temp_addr->ifa_netmask)->sin_addr.s_addr)
								] 
							];
						} else if ( amExpanded ) {
							if ( [adrs6 objectForKey:ifName] == nil ){
								[adrs6 setObject:[[NSMutableArray alloc] init] forKey:ifName];
							}
							// See https://stackoverflow.com/questions/33125710/how-to-get-ipv6-interface-address-using-getifaddr-function/33127330#33127330
							char addr[INET6_ADDRSTRLEN];
							inet_ntop(AF_INET6, &((struct sockaddr_in6*)temp_addr->ifa_addr)->sin6_addr, addr, sizeof(addr));

							[[adrs6 objectForKey:ifName] addObject: [NSString stringWithFormat:@"%@ /%d",
								[NSString stringWithUTF8String:addr], 
								mask6_as_int( ((struct sockaddr_in6*)temp_addr->ifa_netmask)->sin6_addr.s6_addr )
								] 
							];
						}
					} // if (![ifName isEqualToString:@""]){

				} // if ( (temp_addr->ifa_addr->sa_family == AF_INET) || (temp_addr->ifa_addr->sa_family == AF_INET6) ){

			} // if((temp_addr->ifa_addr)){
			temp_addr = temp_addr->ifa_next;
		} // while(temp_addr != NULL) {
	} // if (success == 0) {
	freeifaddrs(interfaces);

	NSMutableDictionary *finalAddrs = [[NSMutableDictionary alloc] init];

	// IPv4 addresses 1st
	[adrs4 enumerateKeysAndObjectsUsingBlock:^(NSString *ifn, NSArray *adrs, BOOL *stop){
		[finalAddrs setObject:adrs forKey:ifn];
		cnt4 += [adrs count];
	}];
	if ( amExpanded ){
		// IPv6 addresses 2nd
		[adrs6 enumerateKeysAndObjectsUsingBlock:^(NSString *ifn, NSArray *adrs, BOOL *stop){
			[finalAddrs setObject: [finalAddrs objectForKey:ifn] == nil ? adrs : [[finalAddrs objectForKey:ifn] arrayByAddingObjectsFromArray:adrs] forKey:ifn];
			cnt6 += [adrs count];
		}];
	}

//bhLogIoC(@"ipAddressList ifs: %@", [finalAddrs allKeys]); //// debug
	[self saveSectionNames:finalAddrs];
	ifsAndAdrs = [NSDictionary dictionaryWithDictionary:finalAddrs];

} // - (void)ipAddressList { // v.1.5

// main idea from https://stackoverflow.com/questions/26524262/how-to-display-all-interfaces-ipv6-prefix-addresses-in-c
// feed in ((struct sockaddr_in6 *)ifa->ifa_netmask)->sin6_addr.s6_addr
static int mask6_as_int(unsigned char *c){
	int i = 0, j = 0;
	unsigned char n = 0;
	while (i < 16) {
		n = c[i];
		while (n > 0) {
			if (n & 1) j++;
			n = n/2;
		}
		i++;
	}
	return j;
}
// feed in ((struct sockaddr_in *)ifa->ifa_netmask)->sin_addr.s_addr
static int mask4_as_int(uint32_t n){
	int i = 0;
	while (n > 0) {
		if (n & 1) i++;
		n = n >> 1;
	}
	return i;
}
// some ideas could be at
// https://github.com/nmav/ipcalc/blob/master/ipcalc.c

@end


@implementation IPonCC 
@synthesize contentViewController;
- (IPonCcViewController *)contentViewController {
	if ( contentViewController == nil ){
		//contentViewController = [[IPonCcViewController alloc] init];
		contentViewController = [[IPonCcViewController alloc] initWithToggleModule:self]; // since I couldn't find a way to access the CCUIToggleModule itself from the UIViewController
	}
	return contentViewController;
}
@end

// Blanxd.H @2018: if I don't have BBEdit at hand I use emacs (nox) if at all possible, and tabs.
// Local Variables:
// mode: objc
// tab-width: 4
// c-basic-offset: 4
// indent-tabs-mode: t
// End:
