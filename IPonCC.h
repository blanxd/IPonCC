// this is quite a stripped down selection of the headers, just for this tweak 
#import <ControlCenterUIKit/CCUIToggleModule.h>

/**
  if the above is included in ${THEOS}/vendor/include/ControlCenterUIKit/
  (so Theos uses those instead of what we have in our headers folder)
  it might still not contain everything we want to use, the ones usually
  published only define a small subset of the available props and methods.
  **/
#ifndef IPonCCheaders
@interface CCUIContentModuleContext : NSObject
-(void)requestExpandModule;
@end
#endif

@interface CCUIToggleModule (IPonCC)
@property (nonatomic,retain) CCUIContentModuleContext *contentModuleContext;                                              //@synthesize contentModuleContext=_contentModuleContext - In the implementation block
- (CCUIContentModuleContext *)contentModuleContext;
@end
/**/

/* colors */
@interface UIColor (sysColors)
+(UIColor*)systemGrayColor; // this is since iOS7
+(UIColor*)labelColor; // this is since iOS13
@end

/* corner radius */
@interface MTMaterialView : UIView
-(CGFloat)_continuousCornerRadius;
@end

/* corner radius */
@interface CCUIContentModuleContentContainerView : UIView {
	MTMaterialView* _moduleMaterialView;
}
-(MTMaterialView *)moduleMaterialView;
@end

/* corner radius */
@interface CCUIContentModuleContainerViewController : UIViewController 
@property (nonatomic,retain) CCUIContentModuleContentContainerView* contentContainerView;
-(double)_continuousCornerRadiusForExpandedState; // 13,14
-(double)_continuousCornerRadiusForCompactState; // 13,14
@end

@class IPonCC;

@interface IPonCcViewController : UIViewController <CCUIContentModuleContentViewController, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic,readonly) CGFloat preferredExpandedContentHeight;
@property (nonatomic,readonly) CGFloat preferredExpandedContentWidth;

@property (nonatomic,assign) CCUIToggleModule* toggleModule;

- (instancetype)initWithToggleModule:(CCUIToggleModule *)givenToggleModule;
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;

-(void)ipLayout;

@end

@interface IPonCC : CCUIToggleModule
//@property (nonatomic,retain) CCUIContentModuleContext* contentModuleContext;
@property (nonatomic,readonly) IPonCcViewController* contentViewController;
//-(id)contentViewController;
@end
