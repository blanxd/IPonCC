# IPonCC
Shows all sorts of IP addresses for the device in the iOS Control Center. Cellular, WiFi and VPN (and everything else), both IPv4 and IPv6.

By default this Makefile builds it for rootless jbs (since IPonCC ver.1.6), with the default toolchain on the system. To build for rootful environments and/or use the old ABI, append RF=1 to the make commandline, eg. `make RF=1` or `make package FINALPACKAGE=1 RF=1`, in this case it tries to find an Xcode 11 toolchain for the job.  
A recent Theos is recommended (post 2023-03-26) but it also does the rootless job with older Theos versions.

**[Version 1.6](https://gitlab.com/blanxd/IPonCC/-/releases#v1.6) (2023-05-21)**

* Added MAC addresses.
* Added rootless support
* Maybe better centering of the IP button while using other CC tweaks.
* Possibly a more solid respring request for all package managers.

**[Version 1.5](https://gitlab.com/blanxd/IPonCC/-/releases#v1.5) (2021-07-08)**

* Better detection of Ethernet interfaces.
* Fixed a few visual glitches on iOS 13 & 14.
* No more "hooking" into anything.
* Moved the button to the bottom in the expanded view.
* Added the addresses' counts on the bottom of the expanded view, just a simple "why not".
* A lot of code cleanup, which consequently resulted in several internal optimizations.

**[Version 1.4.6](https://gitlab.com/blanxd/IPonCC/-/releases#v1.4.6) (2020-01-26)**

* Fixed a bug which could crash the SpringBoard in some rare circumstances (if there were no real IP-addresses except for lo and such, while returning from the detailed expanded view to the 3x1 view).

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.4.5](https://gitlab.com/blanxd/IPonCC/-/releases#v1.4.5) (2019-06-10)**

* Improved the detection of VPN connections.
* Fixed a bug which, on rare occasions, left the tweak polling for IP changes on the background (if the expanded view of IPonCC was kept on screen while the device auto-locked).

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.4.4](https://gitlab.com/blanxd/IPonCC/-/releases#v1.4.4) (2019-03-30)**

* Added arm64e slice (ie. ARMv8.3-A), ie. support for late 2018 and newer devices with A12/A12X Bionic CPU.

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.4.3](https://gitlab.com/blanxd/IPonCC/-/releases#v1.4.3) (2019-02-26)**

* Fixed compatibility with the newer Cydia Substrate (visual effects really, the main functionality hadn't been affected much).
* Added icons for the rows in the Control Center.

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.4.2](https://gitlab.com/blanxd/IPonCC/-/releases#v1.4.2) (2018-07-09)**

* Fixed a crash related to Airplane mode and using the 3x1 module (thank you [/u/Sami_Gharbi](https://www.reddit.com/user/Sami_Gharbi) for reporting)

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.4.1](https://gitlab.com/blanxd/IPonCC/-/releases#v1.4.1) (2018-07-06)**

* 11.0 - 11.3.1 support (tested on 11.3.1 with @Jakeashacks's [rootlessJB](https://github.com/jakeajames/rootlessJB) )
* Fixed a crash when expanding too fast after copying an address from the 3x1 sized module (thank you [/u/Sami_Gharbi](https://www.reddit.com/user/Sami_Gharbi) for reporting)

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.4](https://gitlab.com/blanxd/IPonCC/-/releases#v1.4) (2018-07-05)**

* More accurate "Human Readable" interface names
* 11.0 - 11.3.1 support (tested on 11.3.1 with @Jakeashacks's [rootlessJB](https://github.com/jakeajames/rootlessJB) )

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.3](https://gitlab.com/blanxd/IPonCC/-/releases#v1.3) (2018-06-04)**

This is the way it was supposed to work in the 1st place:                                                                                                                                             
* Adds a separate "IP Addresses" item to the list of CC modules, which displays the addresses straight on the CC (3x1 size). 3D/long-touch on the module shows the expanded list with IPv6 addresses.
* The "IP Button" item in the module list can be used if there's no room for a 3x1 sized item in your CC, need to tap it to see the addresses.
* Adds a button in the expanded view for choosing whether to show a regular list or the internal unfiltered address list.

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.2](https://gitlab.com/blanxd/IPonCC/-/releases#v1.2) (2018-05-10)**
* 3D-touch now shows everything unfiltered, all interfaces by their BSD names.
* Internal optimizations, [CCSupport](https://github.com/opa334/CCSupport) is now a dependency.

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.1](https://gitlab.com/blanxd/IPonCC/-/releases#v1.1) (2018-04-14)**
* Functionality remains as in version 1.0
* Completely removed the dependency for Silo. It now only depends on iOS11 and MobileSubstrate of course.

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**[Version 1.0](https://gitlab.com/blanxd/IPonCC/-/releases#v1.0) (2018-04-07)**
* The version 1.0 is live now, no need to close/open it to see new data (not live technically, polling).
* No need to 3D-press it any more, a simple tap will do.
* The presentation is formatted better visually.
* Tapping any IP address copies it to the pasteboard.

The public source files here remain the very alpha ones (ver.0.0.1), simply gathering the IP info and it only works by 3D-pressing the toggle.

**Version 0.0.1 (2018-03-21)**

The toggle doesn't toggle anything, 3D-pressing it shows the info. It updates the info each time the toggle is 3D-touched, not live.
In order to compile this, one needs the headers and Frameworks dirs from [SiloToggleModule example](https://github.com/ioscreatix/SiloToggleModule) by [ioscreatix](https://github.com/ioscreatix), and theos of course.

The compiled .deb is 64-bit only. iOS >= 11.
